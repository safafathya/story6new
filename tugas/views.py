from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import Post

# Create your views here.
def home(request):
	status = Post.objects.all()
	post_form = PostForm(request.POST or None)

	if request.method == "POST":
		if post_form.is_valid():
			post_form.save()
	
			return redirect('index')

	context = {
			'post_form':post_form,
			'status':status,
		}

	return render(request, 'index.html', context)

