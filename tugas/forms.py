from django import forms
from .models import Post


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['status',]

        widgets = {
                'status' : forms.TextInput(
                    attrs = {
                        'class' : 'form-control',
                        'placeholder' : 'tulis status anda',
                    }
                ),
            }