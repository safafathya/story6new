$(document).ready(function() {
  // $('#about').css('display', 'none');
  // $('#back').css('display', 'none');
  $('#about').hide();
  $('#back').hide();
  $('#theme').css('background-color', '#F49E13');
  $('#knowme').css('background-color', '#F49E13');
  $('#back').css('background-color', '#F49E13');
  var allPanels = $('.panel').hide();
  var active;
  var clicked = false;

  $('.overlay').delay(2000);
  $('.overlay').slideUp(500);

  $(document).on('click', '.accordion', function() {
    allPanels.slideUp();
    if(this === active){
      $(this).next().slideUp();
      $(this).removeClass('active');
      active = null;
      return false;
    }
    $(active).removeClass('active');
    $(this).addClass('active');
    $(this).next().slideDown();
    active = this;
    return false;
  });

  $('#theme').on('click', function() {
      if(clicked){
        // Dark Mode
        $('#theme > h4').text('Bright mode'); 
        $(this).css('background-color', '#F49E13');
        $('#knowme').css('background-color', '#F49E13');
        $('#back').css('background-color', '#F49E13');
        $('body').css('background-color', '#043647');
        $('#about').css('background-color', '#043647');
        $('.judul > h1').css('color', '#F49E13');
        $('input[name="status"]').css('border', '5px solid #F49E13');
        $('input[type="submit"]').css('background', '#F49E13');
        $('.view-status > h1, .list-status div > h3').css('color', '#F49E13');
        $('.view-status > hr').css('border-color', '#FFF1D0');
        $('.list-status div > p').css('color', '#FFF1D0');

        // about
        $('.title > h1').css('color', '#F49E13');
        $('.title > hr').css('border-color', '#FFF1D0');
        $('.form-group > label').css('color', '#F49E13');
        $('.form-group > .input-group').css('color', '#FFF1D0');
        $('.biodata > p').css('color', '#FFF1D0');

        //accordion
        $('.accordion').css('background', '#F49E13');
        //$('.accordion:hover').css('background', '#F4B44B');
        $('.panel').css('background-color', '#FFF1D0');
        $('.panel > h4').css('color', '#043647');

        //app book
        $('input[name="search-box"]').css('border', '5px solid #F49E13');
        $('.table > thead').css('color', '#F49E13');
        $('.table > tbody').css('color', '#FFF1D0');
        
        clicked = false;
      }else{
        //Bright Mode
        $('#theme > h4').text('Dark mode');
        $(this).css('background-color', '#043647');
        $('#knowme').css('background-color', '#043647');
        $('#back').css('background-color', '#043647');
        $('body').css('background-color', '#FFF6E2');
        $('#about').css('background-color', '#FFF6E2');
        $('.judul > h1').css('color', '#043647');
        $('input[name="status"]').css('border', '5px solid #043647');
        $('input[type="submit"]').css('background', '#043647');
        $('.view-status > h1, .list-status div > h3').css('color', '#043647');
        $('.view-status > hr').css('border-color', '#CE8002');
        $('.list-status div > p').css('color', '#CE8002');

        // about
        $('.title > h1').css('color', '#043647');
        $('.title > hr').css('border-color', '#CE8002');
        $('.form-group > label').css('color', '#043647');
        $('.form-group > .input-group, p').css('color', '#CE8002');
        $('.biodata > p').css('color', '#CE8002');

        //accordion
        $('.accordion').css('background', '#043647');
        //$('.accordion:hover').css('background', '#075975');
        $('.panel').css('background-color', 'white');
        $('.panel > h4').css('color', '#CE8002');

        //app book
        $('input[name="search-box"]').css('border', '5px solid #043647');
        $('.table > thead').css('color', '#043647');
        $('.table > tbody').css('color', '#CE8002');

        clicked = true;
      };
      return false;
  });



   $('#knowme').on('click', function() {
     $('#landing-page').hide();
     $(this).hide();
     $('#about').show();
     $('#back').css('display', 'inline-block');
   });

   $('#back').on('click', function() {
     $('#landing-page').show();
     $(this).hide();
     $('#knowme').css('display', 'inline-block');
     $('#about').hide();
     $('#back').hide();
   });

});
