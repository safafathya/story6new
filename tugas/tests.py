from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import Post
from .forms import PostForm
from django.apps import apps
from tugas.apps import TugasConfig
from datetime import date
import time

class Story6UnitTest(TestCase):

	#test url
	def test_story_6_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	def test_story_6_using_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	#test views
	def test_tugas_content_is_written(self):
		response = Client().get('/')
		self.assertIsNotNone(response)

	def test_tugas_is_completed(self):
		request = HttpRequest()
		response = home(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)
		self.assertIn('Get to know me!', html_response)

	#test models
	def test_model_create_new_status(self):
		new_status = Post.objects.create(status='Models UnitTest', waktu=timezone.now())
		counting_all_status = Post.objects.all().count()
		self.assertEqual(counting_all_status, 1)

	def test_model_check_str_is_status(self):
		new_status = Post.objects.create(status='Cek String', waktu=timezone.now())
		self.assertEqual(str(new_status), new_status.status)

	#test forms
	def test_forms_is_valid(self):
		data_form = {'status': 'form UnitTest'}
		form = PostForm(data=data_form)
		self.assertTrue(form.is_valid())

	def test_forms_is_not_valid(self):
		data_form = {'status': 'Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300. Panjang status ini lebih dari 300.'}
		form = PostForm(data=data_form)
		self.assertFalse(form.is_valid())

	#test apps
	def test_apps(self):
		self.assertEqual(TugasConfig.name, 'tugas')
		self.assertEqual(apps.get_app_config('tugas').name, 'tugas')


# class Story6FunctionalTest(LiveServerTestCase):
# 	def setUp(self):
# 		super(Story6FunctionalTest, self).setUp()
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
	

# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(Story6FunctionalTest, self).tearDown()


# 	def test_input_todo(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url) 
# 		self.assertIn('Belum ada status', selenium.page_source)
# 		self.assertNotIn('Coba Coba', selenium.page_source)
# 		status = selenium.find_element_by_id('id_status')
# 		submit = selenium.find_element_by_id('submit')
# 		status.send_keys('Coba Coba')
# 		time.sleep(2)
# 		submit.send_keys(Keys.RETURN)
# 		time.sleep(2)
# 		self.assertIn('Coba Coba', selenium.page_source)
# 		self.assertNotIn('Belum ada status', selenium.page_source)


# 	def test_change_mode(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)

# 		# test mode home
# 		body = selenium.find_element_by_tag_name('body')
# 		text_halo = selenium.find_element_by_id('halo')
# 		submit = selenium.find_element_by_id('submit')	
# 		status = selenium.find_element_by_id('status')
# 		hr_status = selenium.find_element_by_id('hr-status')
# 		nama_status = selenium.find_element_by_id('text-status')

# 		background1 = body.value_of_css_property('background')
# 		halo_color1 = text_halo.value_of_css_property('color')
# 		submit_background1 = submit.value_of_css_property('background')
# 		status_color1 = status.value_of_css_property('color')	
# 		hr_color1 = hr_status.value_of_css_property('border-color')
# 		nama_color1 = nama_status.value_of_css_property('color')

# 		time.sleep(2)
# 		mode_button = selenium.find_element_by_id('theme')
# 		mode_button.click()
# 		time.sleep(2)

# 		background2 = body.value_of_css_property('background')
# 		halo_color2 = text_halo.value_of_css_property('color')
# 		submit_background2 = submit.value_of_css_property('background')
# 		status_color2 = status.value_of_css_property('color')
# 		hr_color2 = hr_status.value_of_css_property('border-color')
# 		nama_color2 = nama_status.value_of_css_property('color')

# 		self.assertNotEqual(background1, background2)
# 		self.assertNotEqual(halo_color1, halo_color2)
# 		self.assertNotEqual(submit_background1, submit_background2)
# 		self.assertNotEqual(status_color1, status_color2)
# 		self.assertNotEqual(hr_color2, hr_color1)
# 		self.assertNotEqual(nama_color1, nama_color2)

# 		# test mode about
# 		time.sleep(2)
# 		knowme_button = selenium.find_element_by_id('knowme')
# 		knowme_button.click()
# 		time.sleep(2)

# 		body_knowme = selenium.find_element_by_tag_name('body')
# 		text_knowme = selenium.find_element_by_id('title-knowme')
# 		hr_knowme = selenium.find_element_by_id('hr-knowme')
# 		label = selenium.find_element_by_tag_name('label')
# 		isi = selenium.find_element_by_id('isi')
# 		desc = selenium.find_element_by_id('desc')
# 		accord = selenium.find_element_by_class_name('accordion')
# 		panel = selenium.find_element_by_class_name('panel')
# 		text_panel = selenium.find_element_by_id('text-panel')

# 		background1 = body_knowme.value_of_css_property('background')
# 		knowme_color1 = text_knowme.value_of_css_property('color')	
# 		hr_color1 = hr_knowme.value_of_css_property('border-color')
# 		label_color1 = label.value_of_css_property('color')
# 		isi_color1 = isi.value_of_css_property('color')
# 		desc_color1 = desc.value_of_css_property('color')
# 		accord_color1 = accord.value_of_css_property('background')
# 		panel_color1 = panel.value_of_css_property('background-color')
# 		text_panel_color1 = text_panel.value_of_css_property('color')

# 		time.sleep(2)
# 		mode_button = selenium.find_element_by_id('theme')
# 		mode_button.click()
# 		time.sleep(2)

# 		background2 = body_knowme.value_of_css_property('background')
# 		knowme_color2 = text_knowme.value_of_css_property('color')	
# 		hr_color2 = hr_knowme.value_of_css_property('border-color')
# 		label_color2 = label.value_of_css_property('color')
# 		isi_color2 = isi.value_of_css_property('color')
# 		desc_color2 = desc.value_of_css_property('color')
# 		accord_color2 = accord.value_of_css_property('background')
# 		panel_color2 = panel.value_of_css_property('background-color')
# 		text_panel_color2 = text_panel.value_of_css_property('color')

# 		self.assertNotEqual(background1, background2)
# 		self.assertNotEqual(knowme_color1, knowme_color2)
# 		self.assertNotEqual(hr_color1, hr_color2)
# 		self.assertNotEqual(label_color1, label_color2)
# 		self.assertNotEqual(isi_color2, isi_color1)
# 		self.assertNotEqual(desc_color1, desc_color2)
# 		self.assertNotEqual(accord_color2, accord_color1)
# 		self.assertNotEqual(panel_color1, panel_color2)
# 		self.assertNotEqual(text_panel_color1, text_panel_color2)
		
		
# 	def test_accordion(self):
# 		selenium = self.selenium
# 		selenium.get(self.live_server_url)

# 		time.sleep(2)
# 		knowme_button = selenium.find_element_by_id('knowme')
# 		knowme_button.click()
# 		time.sleep(2)

# 		accordion = selenium.find_element_by_class_name('accordion')
# 		panel = selenium.find_element_by_class_name('panel')

# 		display1 = panel.value_of_css_property('display')
# 		self.assertEqual(display1, 'none')

# 		time.sleep(2)
# 		accordion.click()
# 		time.sleep(2)

# 		display2 = panel.value_of_css_property('display')
# 		self.assertEqual(display2, 'block')
