function findBook() {

    var searchbox = document.getElementById('searchbox').value
    document.getElementById('listbooks').innerHTML = ""
 
    $.ajax({

        url:"https://www.googleapis.com/books/v1/volumes?q=" + searchbox,
        dataType: "json",

        success: function(data) {

          if(data.totalItems == 0){
            listbooks.innerHTML += "<h3>Tidak terdapat buku dengan keyword tersebut.</h3>"
          }     

          else{
            for (i = 0; i < data.items.length ; i++) {
              if(data.items[i].volumeInfo.imageLinks != null){

              listbooks.innerHTML += "<tr>" +
                                      "<td>" + data.items[i].volumeInfo.title + "</td>" +
                                      "<td><img src= ' " + data.items[i].volumeInfo.imageLinks.smallThumbnail + " ' </td>" +
                                      "<td>" + data.items[i].volumeInfo.authors + "</td>" +
                                      "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                                    "</tr>"

              }    
            }
          }

        },
        type: 'GET'
    });

}

document.getElementById('searchbutton').addEventListener('click', findBook, false)

$.ajax({

    url:"https://www.googleapis.com/books/v1/volumes?q=En arth",
    dataType: "json",

    success: function(data) {
      
      for (i = 0; i < data.items.length ; i++) {

          if(data.items[i].volumeInfo.imageLinks != null){

          listbooks.innerHTML += "<tr>" +
                                      "<td>" + data.items[i].volumeInfo.title + "</td>" +
                                      "<td><img src= ' " + data.items[i].volumeInfo.imageLinks.smallThumbnail + " ' </td>" +
                                      "<td>" + data.items[i].volumeInfo.authors + "</td>" +
                                      "<td>" + data.items[i].volumeInfo.publisher + "</td>" +
                                    "</tr>"

          }
      }
    },
    type: 'GET'
});

var clicked = false;
$('#theme').on('click', function() {
      if(clicked){
        // Dark Mode
        $('input[name="search-box"]').css('border', '5px solid #F49E13');
        $('.table > thead').css('color', '#F49E13');
        $('.table > tbody').css('color', '#FFF1D0');
        
        clicked = false;
      }else{
        //Bright Mode
        $('input[name="search-box"]').css('border', '5px solid #043647');
        $('.table > thead').css('color', '#043647');
        $('.table > tbody').css('color', '#CE8002');

        clicked = true;
      };
      return false;
});
