from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from django.apps import apps
from book.apps import BookConfig


class Story8UnitTest(TestCase):

	#test url
	def test_story_8_url_is_exist(self):
		response = Client().get('/book/')
		self.assertEqual(response.status_code, 200)

	def test_story_8_using_index_template(self):
		response = Client().get('/book/')
		self.assertTemplateUsed(response, 'book.html')

	def test_story_8_using_home_func(self):
		found = resolve('/book/')
		self.assertEqual(found.func, book)

	#test views
	def test_book_content_is_written(self):
		response = Client().get('/book/')
		self.assertIsNotNone(response)

	def test_book_is_completed(self):
		request = HttpRequest()
		response = book(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Find your favorite books!', html_response)

	#test apps
	def test_apps_book(self):
		self.assertEqual(BookConfig.name, 'book')
		self.assertEqual(apps.get_app_config('book').name, 'book')
