
from django.test import TestCase, Client, RequestFactory           
from django.urls import resolve, reverse                            
from django.test import LiveServerTestCase
from django.shortcuts import render, resolve_url
from django.contrib.auth.models import User, AnonymousUser

from django.http.response import HttpResponseRedirect
from django.conf import settings
from importlib import import_module
from django.http import HttpRequest
from django.utils import timezone
from django.apps import apps
from login.apps import LoginConfig

from .views import *

class Story9UnitTest(TestCase):

	def test_home_url(self):
		response=Client().get('/home/')
		self.assertEqual(response.status_code, 302)

	def test_login_url(self):
		response=Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_logout_url(self):
		response=Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_login_using_template(self):
		response=Client().get('/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_home_using_home_func(self):
		found=resolve('/home/')
		self.assertEqual(found.func, home)

	def test_login_using_loginform_func(self):
		found=resolve('/login/')
		self.assertEqual(found.func, loginform)

	def test_logout_using_logoutform_func(self):
		found=resolve('/logout/')
		self.assertEqual(found.func, logoutform)

	def test_login_content(self):
		response = self.client.get('/login/')
		self.assertContains(response, 'Login')

	def test_apps_login(self):
		self.assertEqual(LoginConfig.name, 'login')
		self.assertEqual(apps.get_app_config('login').name, 'login')

	def test_login(self):
		username=User.objects.create(username='satu')
		username.set_password('111')
		username.save()
		sudah_login = Client().login(username='satu', password='111')
		self.assertTrue(sudah_login, True)

	def setUp(self):
		self.user = User.objects.create_user(
			username='kak asdos',
			email='asdos@gmail.com',
			password='asdos123',
			)

	def test_login_page(self):
		response = self.client.post('/login/', follow=True, data={
			'username':'kak asdos',
			'password':'asdos123',
			})
		self.assertRedirects(response, '/home/')
		self.assertContains(response, 'Welcome, kak asdos')






