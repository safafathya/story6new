from django.urls import path

from . import views

urlpatterns = [
    path('login/', views.loginform, name='login'),
    path('home/', views.home, name='home'),
    path('logout/', views.logoutform, name='logout'),
]